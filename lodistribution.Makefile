
where_am_I := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
include $(E3_REQUIRE_TOOLS)/driver.makefile


REQUIRED += asyn

EXCLUDE_ARCHS = linux-ppc64e6500

USR_CFLAGS   += -std=c99
USR_CXXFLAGS += -std=c++11

SRC_BASE:=lodistributionApp/src

USR_INCLUDES += -I$(where_am_I)$(SRC_BASE)

HEADERS += $(SRC_BASE)/LOdistributionAsynPortDriver.h

HEADERS += $(SRC_BASE)/lo-lib/liblo.h
HEADERS += $(SRC_BASE)/lo-lib/xilspi.h

SOURCES += $(SRC_BASE)/lo-lib/liblo.c
SOURCES += $(SRC_BASE)/lo-lib/xilspi.c

SOURCES += $(SRC_BASE)/LOdistributionAsynPortDriver.cpp
SOURCES += $(SRC_BASE)/LOdistributionReg.cpp

DBDS    += $(SRC_BASE)/LOdistributionAsynPortDriverInclude.dbd

SCRIPTS += $(wildcard iocsh/*.iocsh)

TEMPLATES += $(wildcard  $(SRC_BASE)/../Db/*.db)

USR_DBFLAGS += -I . -I ..
USR_DBFLAGS += -I $(EPICS_BASE)/db

vlibs:

.PHONY: vlibs 
