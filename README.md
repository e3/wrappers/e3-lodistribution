e3-lodistribution  
======
ESS Site-specific EPICS module : lodistribution

# Description

The LO RTM device is responsible for the generation of the local oscillator signal used for RF signal down-conversion. 
The LO software is based on the same drivers as Piezo Driver (since they are both using same type AMC module – RTM carrier) but the different API library exposes unit parameters for EPICS IOC.
More detailed description is provdied [here](https://confluence.esss.lu.se/display/HAR/LO+distribution+-+IOC) 

## Requirements

- Xilinx DMA/Bridge Subsystem for PCI Express (XDMA) kernel module

An [rpm package](https://gitlab.esss.lu.se/ics-infrastructure/rpms/xdma-dkms) is available in the rpm-ics repository.
```sh
$ yum install xdma-dkms
```

## EPICS dependencies
- xdma
- asyn

## Installation 

```sh
$ make build
$ make install
```

For further targets, type `make`.

## GUI operator panels
In opi directory, there are operator panels in .bob format. Launch them in Phoebus and adapt macros definitions in `LO Window`. There is no need to set macros in `Expert` window, since they are distributed from first window to next window. That is why you always have to run panels starting with `LO Window`- presented below.  

![Main window](opi/images/main_window.PNG?raw=true)

### Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

### Author Information
In case of problems contact with kklys@mail.dmcs.pl
