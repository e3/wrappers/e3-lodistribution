# 1.1.0
* Use database scanning for updating records
* Improved logging

# 1.0.0
* Initial release
