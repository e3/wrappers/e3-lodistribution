#include "xilspi.h"
#include "liblo.h"
#include <unistd.h>
#include <time.h>

enum XilSPIReg
{
    XSPI_SRR     = 0x40,
    XSPI_SPICR   = 0x60,
    XSPI_SPISR   = 0x64,
    XSPI_SPIDTR  = 0x68,
    XSPI_SPIDRR  = 0x6C,
    XSPI_SPISSR  = 0x70,
};

enum XilSPISpisrBit
{
    XSPI_RX_EMPTY = 0,
    XSPI_RX_FULL = 1,
    XSPI_TX_EMPTY = 2,
    XSPI_TX_FULL = 3,
    XSPI_MODF = 4,
    XSPI_SM_SELECT = 5,
    XSPI_POLPHA_ERROR = 6,
    XSPI_SM_ERROR = 7,
    XSPI_MSB_ERROR = 8,
    XSPI_LOOP_ERROR = 9,
    XSPI_COMMAND_ERROR_BIT = 10,
};


#define SPI_TIMEOUT (CLOCKS_PER_SEC / 5)

int xspi_init(xildev *dev, int32_t base_addr, uint32_t flags)
{
	if(!dev)
		return LO_ERROR_NULL_POINTER;
	uint32_t reg_val = 0;
	if(xil_read_reg(dev, base_addr + XSPI_SPISR, &reg_val) != 0)
		return LO_ERROR_REG_ACCESS;
	if(reg_val == 0xFFFFFFFF)
		return LO_ERROR_REG_ACCESS;
	if(xil_write_reg(dev, base_addr + XSPI_SPICR, flags | (1 << XSPI_TX_RST) | (1 << XSPI_RX_RST)) != 0)
		return LO_ERROR_REG_ACCESS;
	if(xil_read_reg(dev, base_addr + XSPI_SPICR, &reg_val) != 0)
		return LO_ERROR_REG_ACCESS;
	if(reg_val != flags)
		return LO_ERROR_NO_RESPONSE;
	// deselect slaves
	if(xil_write_reg(dev, base_addr + XSPI_SPISSR, ~0) != 0)
		return LO_ERROR_REG_ACCESS;
	uint8_t buf[] = { 0xFF };
	return xspi_exchange(dev, base_addr, buf, buf, sizeof(buf));
}

int xspi_exchange(xildev *dev, int32_t base_addr, uint8_t * tx, uint8_t * rx, unsigned len)
{
	if(!dev || !tx)
		return LO_ERROR_NULL_POINTER;
	uint32_t reg_val = 0;
	clock_t start = clock();
	// send the data
	unsigned rx_pos = 0, tx_pos = 0;
	while(tx_pos < len)
	{
		if(xil_read_reg(dev, base_addr + XSPI_SPISR, &reg_val) != 0)
			return LO_ERROR_REG_ACCESS;
		if((reg_val & (1 << (int)XSPI_RX_EMPTY)) == 0)
		{	// RX not empty
			if(xil_read_reg(dev, base_addr + XSPI_SPIDRR, &reg_val) != 0)
				return LO_ERROR_REG_ACCESS;
			if(rx && (rx_pos < len))
				rx[rx_pos] = reg_val;
			rx_pos++;
		}
		while(true)
		{	// wait for some space in buffer
			if(xil_read_reg(dev, base_addr + XSPI_SPISR, &reg_val) != 0)
				return LO_ERROR_REG_ACCESS;
			if((reg_val & (1 << (int)XSPI_TX_FULL)) == 0)
				break;
			usleep(10);
			if(clock() - start > SPI_TIMEOUT)
				return LO_ERROR_NO_RESPONSE;
		}
		//perform write
		if(xil_write_reg(dev, base_addr + XSPI_SPIDTR, tx[tx_pos++]) != 0)
			return LO_ERROR_REG_ACCESS;
	}
	// wait for RX to end
	while(rx_pos < len)
	{
		while(true)
		{	// capture RX data, if any
			if(xil_read_reg(dev, base_addr + XSPI_SPISR, &reg_val) != 0)
				return LO_ERROR_REG_ACCESS;
			if((reg_val & (1 << (int)XSPI_RX_EMPTY)) != 0)
				break;
			if(xil_read_reg(dev, base_addr + XSPI_SPIDRR, &reg_val) != 0)
				return LO_ERROR_REG_ACCESS;
			if(rx && (rx_pos < len))
				rx[rx_pos] = reg_val;
			rx_pos++;
		}
		usleep(10);
		if(clock() - start > SPI_TIMEOUT)
			return LO_ERROR_NO_RESPONSE;
	}
	return LO_ERROR_NONE;
}

int xspi_select_slave(xildev *dev, int32_t base_addr, int slave_id)
{
	if(!dev)
		return LO_ERROR_NULL_POINTER;
	if(xil_write_reg(dev, base_addr + XSPI_SPISSR, ~(1 << slave_id)) != 0)
		return LO_ERROR_REG_ACCESS;
	return LO_ERROR_NONE;
}

int xspi_deselect_slave(xildev *dev, int32_t base_addr)
{
	if(!dev)
		return LO_ERROR_NULL_POINTER;
	if(xil_write_reg(dev, base_addr + XSPI_SPISSR, ~0) != 0)
		return LO_ERROR_REG_ACCESS;
	return LO_ERROR_NONE;
}

int xspi_read(xildev *dev, int32_t base_addr, uint8_t * cmd, uint8_t cmd_len, uint8_t * rx, unsigned rx_len, int slave)
{
	if(!dev)
		return LO_ERROR_NULL_POINTER;
	// select the slave
	if(xil_write_reg(dev, base_addr + XSPI_SPISSR, ~(1 << slave)) != 0)
		return LO_ERROR_REG_ACCESS;
	for(unsigned i = 0; i < rx_len; i++)
		rx[i] = 0xFF;
	int status = xspi_exchange(dev, base_addr, cmd, NULL, cmd_len);
	if(rx_len && (status == LO_ERROR_NONE))
		status = xspi_exchange(dev, base_addr, rx, rx, rx_len);
	// deselect the slave
	if(xil_write_reg(dev, base_addr + XSPI_SPISSR, ~0) != 0)
		return LO_ERROR_REG_ACCESS;
	return status;
}

int xspi_write(xildev *dev, int32_t base_addr, uint8_t * cmd, uint8_t cmd_len, uint8_t * tx, unsigned tx_len, int slave)
{
	if(!dev)
		return LO_ERROR_NULL_POINTER;
    // select the slave
	if(xil_write_reg(dev, base_addr + XSPI_SPISSR, ~(1 << slave)) != 0)
		return LO_ERROR_REG_ACCESS;
    int status = xspi_exchange(dev, base_addr, cmd, NULL, cmd_len);
    if(tx_len && (status == LO_ERROR_NONE))
        status = xspi_exchange(dev, base_addr, tx, NULL, tx_len);
    // deselect the slave
	if(xil_write_reg(dev, base_addr + XSPI_SPISSR, ~0) != 0)
		return LO_ERROR_REG_ACCESS;
	return status;
}
