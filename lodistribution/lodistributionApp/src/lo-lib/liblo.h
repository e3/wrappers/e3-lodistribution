#ifndef LIBLO_H_
#define LIBLO_H_

#include "libxildrv.h"
#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

enum PcdErrorType
{
	LO_ERROR_NONE = 0,
	LO_ERROR_REG_ACCESS = -1,
	LO_ERROR_NULL_POINTER = -2,
	LO_ERROR_OUT_OF_RANGE = -3,
	LO_ERROR_NO_RESPONSE = -4,
	LO_WRONG_FIRMWARE_ID = -5,
};

enum LOConfigBits {
	LO_BIT_LE_EN = 0,
	LO_BIT_PARTIAL_RESET_N = 1,
	LO_BIT_IF_SEL = 2,
	LO_BIT_IF_BOOST = 3,
	LO_BIT_CLK_EN = 4,
	LO_BIT_CLK_CFG = 5,
	LO_BIT_POW_STR_SEL = 6,
	LO_BIT_POW_STR = 7,
};

int lo_init(xildev *dev);
const char * lo_error_string(int error_code);

// SPI Communication
int lo_reg_read(xildev *dev, uint8_t reg, uint8_t *value);
int lo_reg_write(xildev *dev, uint8_t reg, uint8_t value);

// Registers access
int lo_config_set_bits(xildev *dev, uint8_t set_mask);
int lo_config_clear_bits(xildev *dev, uint8_t clear_mask);
int lo_config_get(xildev *dev, uint8_t *value);
int lo_atten_set(xildev *dev, uint8_t value);
int lo_atten_get(xildev *dev, uint8_t *value);
int lo_power_get(xildev *dev, uint16_t *clk_pow, uint16_t *lo_pow, uint16_t *ref_pow);
int lo_version_get(xildev *dev, uint16_t *ver);

// Value conversion
float lo_power_in_dbm(uint16_t power_word);

#ifdef __cplusplus
}
#endif

#endif /* LIBLO_H_ */
