#ifndef XILSPI_H
#define XILSPI_H

#include "libxildrv.h"
#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

enum XilSPIFlags
{
    XSPI_LOOP        = 0,
    XSPI_SPE         = 1,
    XSPI_MASTER      = 2,
    XSPI_CPOL        = 3,
    XSPI_CPHA        = 4,
    XSPI_TX_RST      = 5,
    XSPI_RX_RST      = 6,
    XSPI_MANUAL      = 7,
    XSPI_MSTR_INH    = 8,
    XSPI_LSB_FIRST   = 9,
};

#define XSPI_COMMON_FLAGS ((1 << (int)XSPI_SPE) | (1 << (int)XSPI_MASTER) | (1 << (int)XSPI_MANUAL))

int xspi_init(xildev *dev, int32_t base_addr, uint32_t flags);
int xspi_exchange(xildev *dev, int32_t base_addr, uint8_t * tx, uint8_t * rx, unsigned len);
int xspi_select_slave(xildev *dev, int32_t base_addr, int slave_id);
int xspi_deselect_slave(xildev *dev, int32_t base_addr);
int xspi_read(xildev *dev, int32_t base_addr, uint8_t * cmd, uint8_t cmd_len, uint8_t * rx, unsigned rx_len, int slave);
int xspi_write(xildev *dev, int32_t base_addr, uint8_t * cmd, uint8_t cmd_len, uint8_t * tx, unsigned tx_len, int slave);

#ifdef __cplusplus
}
#endif

#endif // XILSPI_H
