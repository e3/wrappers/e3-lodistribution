#include <stdlib.h>
#include <iostream>

using namespace std ;

#include <epicsTypes.h>
#include "LOdistributionAsynPortDriver.h"

const char * P_POWER_TRIG_String = "POWER_TRIG";			// asynInt32
const char * P_CONFIG_TRIG_String = "CONFIG_TRIG";			// asynInt32
const char * P_ATTN_TRIG_String = "ATTN_TRIG";				// asynInt32

const char * P_VERSION_RB_String = "VERSION_RB";			// asynInt32
const char * P_CONFIG_RB_String = "CONFIG_RB";				// asynInt32

const char * P_LO_STATE_RB_String = "LO_STATE_RB";			// asynInt32
const char * P_LO_STATE_NAME_RB_String = "LO_STATE_NAME_RB";		// asynOctet
const char * P_LO_STATE_COMMENT_RB_String = "LO_STATE_COMMENT_RB";	// asynOctet
const char * P_PART_RST_RB_String = "PART_RST_RB";			// asynInt32
const char * P_PART_RST_NAME_RB_String = "PART_RST_NAME_RB";		// asynOctet
const char * P_PART_RST_COMMENT_RB_String = "PART_RST_COMMENT_RB";	// asynOctet
const char * P_DIV_RB_String = "DIV_RB";				// asynInt32
const char * P_DIV_NAME_RB_String = "DIV_NAME_RB";			// asynOctet
const char * P_DIV_COMMENT_RB_String = "DIV_COMMENT_RB";		// asynOctet
const char * P_BOOST_TYPE_RB_String = "BOOST_TYPE_RB";			// asynInt32
const char * P_BOOST_TYPE__NAME_RB_String = "BOOST_TYPE_NAME_RB";	// asynOctet
const char * P_BOOST_TYPE_COMMENT_RB_String = "BOOST_TYPE_COMMENT_RB";	// asynOctet
const char * P_CLK_STATE_RB_String = "CLK_STATE_RB";			// asynInt32
const char * P_CLK_STATE_NAME_RB_String = "CLK_STATE_NAME_RB";		// asynOctet 
const char * P_CLK_STATE_COMMENT_RB_String = "CLK_STATE_COMMENT_RB";	// asynOctet
const char * P_DIV_TYPE_RB_String = "DIV_TYPE_RB";			// asynInt32
const char * P_DIV_TYPE_NAME_RB_String = "DIV_TYPE_NAME_RB";		// asynOctet
const char * P_DIV_TYPE_COMMENT_RB_String = "DIV_TYPE_COMMENT_RB";	// asynOctet
const char * P_TRIG_SOUR_RB_String = "TRIG_SOUR_RB";			// asynInt32
const char * P_TRIG_SOUR_NAME_RB_String = "TRIG_SOUR_NAME_RB";		// asynOctet
const char * P_TRIG_SOUR_COMMENT_RB_String = "TRIG_SOUR_COMMENT_RB";	// asynOctet
const char * P_TRIG_RB_String = "TRIG_RB";				// asynInt32
const char * P_TRIG_NAME_RB_String = "TRIG_NAME_RB";			// asynOctet
const char * P_TRIG_COMMENT_RB_String = "TRIG_COMMENT_RB";		// asynOctet

const char * P_ATTENUATION_RB_String = "ATTENUATION_RB";		// asynInt32
const char * P_CLK_POWER_RB_String = "CLK_POWER_RB";			// asynInt32
const char * P_CLK_DBM_RB_String = "CLK_DBM_RB";			// asynFloat64
const char * P_LO_POWER_RB_String = "LO_POWER_RB";			// asynInt32
const char * P_LO_DBM_RB_String = "LO_DBM_RB"; 				// asynFloat64
const char * P_REF_POWER_RB_String = "REF_POWER_RB";			// asynInt32
const char * P_REF_DBM_RB_String = "REF_DBM_RB"; 			// asynFloat64

const char * P_ATTENUATION_SP_String = "ATTENUATION_SP";		// asynInt32
const char * P_LO_STATE_SP_String = "LO_STATE_SP";			// asynInt32
const char * P_PART_RST_SP_String = "PART_RST_SP";			// asynInt32
const char * P_DIV_SP_String = "DIV_SP";				// asynInt32
const char * P_BOOST_TYPE_SP_String = "BOOST_TYPE_SP";			// asynInt32
const char * P_CLK_STATE_SP_String = "CLK_STATE_SP";			// asynInt32
const char * P_DIV_TYPE_SP_String = "DIV_TYPE_SP";			// asynInt32
const char * P_TRIG_SOUR_SP_String = "TRIG_SOUR_SP";			// asynInt32
const char * P_TRIG_SP_String = "TRIG_SP";				// asynInt32

const char * P_DMA_RB_String = "DMA_RB";				// asynOctet
const char * P_ALARM_String = "ALARM";					// asynInt32


LOdistributionAsynPortDriver::LOdistributionAsynPortDriver(const char *portName, int maxPoints, const char *devName)
	:asynPortDriver(portName, 
			1,
			asynInt32Mask | asynFloat64Mask | asynOctetMask | asynDrvUserMask,
			asynInt32Mask | asynFloat64Mask | asynOctetMask,
			0,
			1,
			0,
			0)
{
	createParam(P_ATTN_TRIG_String, asynParamInt32, &P_ATTN_TRIG);
	createParam(P_CONFIG_TRIG_String, asynParamInt32, &P_CONFIG_TRIG);
	createParam(P_POWER_TRIG_String, asynParamInt32, &P_POWER_TRIG);

	createParam(P_VERSION_RB_String, asynParamInt32, &P_VERSION_RB);
	createParam(P_CONFIG_RB_String, asynParamInt32, &P_CONFIG_RB);

	createParam(P_LO_STATE_RB_String, asynParamInt32, &P_LO_STATE.VAL_RB);
	createParam(P_LO_STATE_NAME_RB_String, asynParamOctet, &P_LO_STATE.NAME_RB);
	createParam(P_LO_STATE_COMMENT_RB_String, asynParamOctet, &P_LO_STATE.COMMENT_RB);
	createParam(P_PART_RST_RB_String, asynParamInt32, &P_PART_RST.VAL_RB);
	createParam(P_PART_RST_NAME_RB_String, asynParamOctet, &P_PART_RST.NAME_RB);
	createParam(P_PART_RST_COMMENT_RB_String, asynParamOctet, &P_PART_RST.COMMENT_RB);
	createParam(P_DIV_RB_String, asynParamInt32, &P_DIV.VAL_RB);
	createParam(P_DIV_NAME_RB_String, asynParamOctet, &P_DIV.NAME_RB);
	createParam(P_DIV_COMMENT_RB_String, asynParamOctet, &P_DIV.COMMENT_RB);
	createParam(P_BOOST_TYPE_RB_String, asynParamInt32, &P_BOOST_TYPE.VAL_RB);
	createParam(P_BOOST_TYPE__NAME_RB_String, asynParamOctet, &P_BOOST_TYPE.NAME_RB);
	createParam(P_BOOST_TYPE_COMMENT_RB_String, asynParamOctet, &P_BOOST_TYPE.COMMENT_RB);
	createParam(P_CLK_STATE_RB_String, asynParamInt32, &P_CLK_STATE.VAL_RB);
	createParam(P_CLK_STATE_NAME_RB_String, asynParamOctet, &P_CLK_STATE.NAME_RB);
	createParam(P_CLK_STATE_COMMENT_RB_String, asynParamOctet, &P_CLK_STATE.COMMENT_RB);
	createParam(P_DIV_TYPE_RB_String, asynParamInt32, &P_DIV_TYPE.VAL_RB);
	createParam(P_DIV_TYPE_NAME_RB_String, asynParamOctet, &P_DIV_TYPE.NAME_RB);
	createParam(P_DIV_TYPE_COMMENT_RB_String, asynParamOctet, &P_DIV_TYPE.COMMENT_RB);
	createParam(P_TRIG_SOUR_RB_String, asynParamInt32, &P_TRIG_SOUR.VAL_RB);
	createParam(P_TRIG_SOUR_NAME_RB_String, asynParamOctet, &P_TRIG_SOUR.NAME_RB);
	createParam(P_TRIG_SOUR_COMMENT_RB_String, asynParamOctet, &P_TRIG_SOUR.COMMENT_RB);
	createParam(P_TRIG_RB_String, asynParamInt32, &P_TRIG.VAL_RB);
	createParam(P_TRIG_NAME_RB_String, asynParamOctet, &P_TRIG.NAME_RB);
	createParam(P_TRIG_COMMENT_RB_String, asynParamOctet, &P_TRIG.COMMENT_RB);

	createParam(P_ATTENUATION_RB_String, asynParamInt32, &P_ATTENUATION_RB);
	createParam(P_CLK_POWER_RB_String, asynParamInt32, &P_CLK_POWER_RB);
	createParam(P_CLK_DBM_RB_String, asynParamFloat64, &P_CLK_DBM_RB);
	createParam(P_LO_POWER_RB_String, asynParamInt32, &P_LO_POWER_RB);
	createParam(P_LO_DBM_RB_String, asynParamFloat64, &P_LO_DBM_RB);
	createParam(P_REF_POWER_RB_String, asynParamInt32, &P_REF_POWER_RB);
	createParam(P_REF_DBM_RB_String, asynParamFloat64, &P_REF_DBM_RB);

	createParam(P_ATTENUATION_SP_String, asynParamInt32, &P_ATTENUATION_SP);
	createParam(P_LO_STATE_SP_String, asynParamInt32, &P_LO_STATE.VAL_SP);
	createParam(P_PART_RST_SP_String, asynParamInt32, &P_PART_RST.VAL_SP);
	createParam(P_DIV_SP_String, asynParamInt32, &P_DIV.VAL_SP);
	createParam(P_BOOST_TYPE_SP_String, asynParamInt32, &P_BOOST_TYPE.VAL_SP);
	createParam(P_CLK_STATE_SP_String, asynParamInt32, &P_CLK_STATE.VAL_SP);
	createParam(P_DIV_TYPE_SP_String, asynParamInt32, &P_DIV_TYPE.VAL_SP);
	createParam(P_TRIG_SOUR_SP_String, asynParamInt32, &P_TRIG_SOUR.VAL_SP);
	createParam(P_TRIG_SP_String, asynParamInt32, &P_TRIG.VAL_SP);
	createParam(P_DMA_RB_String, asynParamOctet, &P_DMA_RB);
	createParam(P_ALARM_String, asynParamInt32, &P_ALARM);
	
	// initialize all arrays
	initArrays();

	// initialize device
        dev = xil_open_device(devName);
	if (!dev)
	{
		asynPrint(pasynUserSelf, ASYN_TRACE_ERROR, "Can't connect to device\n");
	}
	else
	{
		int init_s = lo_init(dev);
		if(init_s != LO_ERROR_NONE)
		{
			asynPrint(pasynUserSelf, ASYN_TRACE_ERROR, "%s\n", lo_error_string(init_s));
		}
	}

	// set device name
	setStringParam(P_DMA_RB, devName);

	int res;
	uint16_t version;

	// Firmware version
	res = lo_version_get(dev, &version);
	if(res == LO_ERROR_NONE)
	{
		setIntegerParam(P_VERSION_RB, version);
	}
	AlarmProcedure(res);

	// initialize parameters
	readConfig(array_bits_val_sp);
	readAttenuation(P_ATTENUATION_SP);
}

void LOdistributionAsynPortDriver::readConfig(int *array_value)
{
	int res;
	uint8_t config;
	res = lo_config_get(dev, &config);

	if(res == LO_ERROR_NONE)
	{
		for(int i = 0; i < NUM_BITS; i++)
		{
			int value = (config >> i) & 1;
			const char *comment = value ? bits[i].on_comment : bits[i].off_comment;
			setStringParam(array_bits_comment[i], comment ? comment : "");
			setStringParam(array_bits_name[i], bits[i].name);
			setIntegerParam(array_value[i], value);
		}
		setIntegerParam(P_CONFIG_RB, config);
	}
	AlarmProcedure(res);
}

void LOdistributionAsynPortDriver::readAttenuation(int pv_param)
{
	int res;
	uint8_t atten;
	res = lo_atten_get(dev, &atten);
	if(res == LO_ERROR_NONE)
	{
		setIntegerParam(pv_param, atten);
	}
	AlarmProcedure(res);
}

void LOdistributionAsynPortDriver::readPower()
{
	int res;
	uint16_t clk_pow, lo_pow, ref_pow;
	float clk_dbm, lo_dbm, ref_dbm;

	res = lo_power_get(dev, &clk_pow, &lo_pow, &ref_pow);
	if(res == LO_ERROR_NONE)
	{
		clk_dbm = lo_power_in_dbm(clk_pow);
		lo_dbm = lo_power_in_dbm(lo_pow);
		ref_dbm = lo_power_in_dbm(ref_pow);
		setIntegerParam(P_CLK_POWER_RB, clk_pow);
		setIntegerParam(P_LO_POWER_RB, lo_pow);
		setIntegerParam(P_REF_POWER_RB, ref_pow);
		setDoubleParam(P_CLK_DBM_RB, clk_dbm);
		setDoubleParam(P_LO_DBM_RB, lo_dbm);
		setDoubleParam(P_REF_DBM_RB, ref_dbm);
	}
	AlarmProcedure(res);
}

void LOdistributionAsynPortDriver::setAttenuation()
{
	asynPrint(pasynUserSelf, ASYN_TRACEIO_DRIVER, "Updating attenuation value\n");
	uint8_t atten;
	int value;
	getIntegerParam(P_ATTENUATION_SP, &value);
	atten = value;
	int res = lo_atten_set(dev, atten);
	AlarmProcedure(res);
}

void LOdistributionAsynPortDriver::setBit(int idx)
{
	int state, res = LO_ERROR_NONE;
	getIntegerParam(array_bits_val_sp[idx], &state);
	if(state == 0)
	{
		res = lo_config_clear_bits(dev, 1 << idx);
	}
	else if(state == 1)
	{
		res = lo_config_set_bits(dev, 1 << idx);
	}

	if(res == LO_ERROR_NONE)
	{
		asynPrint(pasynUserSelf, ASYN_TRACEIO_DRIVER, "Bit %d set to %d\n", idx, state);
	}
	AlarmProcedure(res);
}

asynStatus LOdistributionAsynPortDriver::writeInt32(asynUser *pasynUser, epicsInt32 value)
{
	int function = pasynUser->reason;
	asynStatus status = asynSuccess;
	status = (asynStatus) setIntegerParam(function, value);

	if(function == P_ATTENUATION_SP)
	{
		setAttenuation();
	}

	for(int i=0; i<NUM_BITS; i++)
	{
		if(function == array_bits_val_sp[i])
		{
			setBit(i);
		}
	}

	if(function == P_CONFIG_TRIG)
	{
		readConfig(array_bits_val);
	}

	if(function == P_POWER_TRIG)
	{
		readPower();
	}

	if(function == P_ATTN_TRIG)
	{
		readAttenuation(P_ATTENUATION_RB);
	}

	callParamCallbacks();
	return status;
}

void LOdistributionAsynPortDriver::initArrays()
{
	// initialize arrays with comments
	array_bits_comment[0] = P_LO_STATE.COMMENT_RB;
	array_bits_comment[1] = P_PART_RST.COMMENT_RB;
	array_bits_comment[2] = P_DIV.COMMENT_RB;
	array_bits_comment[3] = P_BOOST_TYPE.COMMENT_RB;
	array_bits_comment[4] = P_CLK_STATE.COMMENT_RB;
	array_bits_comment[5] = P_DIV_TYPE.COMMENT_RB;
	array_bits_comment[6] = P_TRIG_SOUR.COMMENT_RB;
	array_bits_comment[7] = P_TRIG.COMMENT_RB;

	// initialize arrays with names
	array_bits_name[0] = P_LO_STATE.NAME_RB;
	array_bits_name[1] = P_PART_RST.NAME_RB;
	array_bits_name[2] = P_DIV.NAME_RB;
	array_bits_name[3] = P_BOOST_TYPE.NAME_RB;
	array_bits_name[4] = P_CLK_STATE.NAME_RB;
	array_bits_name[5] = P_DIV_TYPE.NAME_RB;
	array_bits_name[6] = P_TRIG_SOUR.NAME_RB;
	array_bits_name[7] = P_TRIG.NAME_RB;

	// initialize arrays with bit values
	array_bits_val[0] = P_LO_STATE.VAL_RB;
	array_bits_val[1] = P_PART_RST.VAL_RB;
	array_bits_val[2] = P_DIV.VAL_RB;
	array_bits_val[3] = P_BOOST_TYPE.VAL_RB;
	array_bits_val[4] = P_CLK_STATE.VAL_RB;
	array_bits_val[5] = P_DIV_TYPE.VAL_RB;
	array_bits_val[6] = P_TRIG_SOUR.VAL_RB;
	array_bits_val[7] = P_TRIG.VAL_RB;

	// initialize arrays with set bit values
	array_bits_val_sp[0] = P_LO_STATE.VAL_SP;
	array_bits_val_sp[1] = P_PART_RST.VAL_SP;
	array_bits_val_sp[2] = P_DIV.VAL_SP;
	array_bits_val_sp[3] = P_BOOST_TYPE.VAL_SP;
	array_bits_val_sp[4] = P_CLK_STATE.VAL_SP;
	array_bits_val_sp[5] = P_DIV_TYPE.VAL_SP;
	array_bits_val_sp[6] = P_TRIG_SOUR.VAL_SP;
	array_bits_val_sp[7] = P_TRIG.VAL_SP;
}


void LOdistributionAsynPortDriver::AlarmProcedure(int result)
{
	setIntegerParam(P_ALARM, abs(result));
	if(result != LO_ERROR_NONE)
	{
		asynPrint(pasynUserSelf, ASYN_TRACE_ERROR, "%s\n", lo_error_string(result));
	}
}

LOdistributionAsynPortDriver::~LOdistributionAsynPortDriver()
{
	asynPrint(pasynUserSelf, ASYN_TRACE_FLOW, "Destructor\n");
	// closing device
	xil_close_device(dev);
}
