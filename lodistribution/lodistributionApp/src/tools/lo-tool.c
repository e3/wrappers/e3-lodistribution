#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>
#include <getopt.h>
#include <ctype.h>
#include <errno.h>
#include <time.h>
#include <unistd.h>
#include <math.h>

#include "liblo.h"

#define NUM_BITS	8

struct BitDescr {
	const char * name;
	const char * on_comment;
	const char * off_comment;
} const bits[NUM_BITS] = {
	{"LO_EN", "LO enabled", "LO disabled"},
	{"PARTIAL_RESET_N", "Reset released", "Reset active"},
	{"IF_SEL", "IF divider 22", "IF divider 28"},
	{"IF_BOOST", "IF: HSTL Boost", "IF: HSTL"},
	{"CLK_EN", "CLK enabled", "CLK disabled"},
	{"CLK_CFG", "alternate dividers (not recommended)", "default dividers"},
	{"POW_STR_SEL", "power measurement triggered by Zone3", "power measurement triggered by POW_STR bit"},
	{"POW_STR", "continuous power readout", "triggered power readout"},
};

void print_usage(char *prog_name)
{
	printf("\n");
	printf("ESS LO Distribution test utility\n");
	printf("\n");
	printf("General arguments:\n");
	printf("    -x <xdma_file> - Use the specified xdma file, default is /dev/xdma0\n");
	printf("    -a <value>     - Set LO attenuation (range 0..31)\n");
	printf("    -s <bit_name>  - Set the specified control bit\n");
	printf("    -c <bit_name>  - Clear the specified control bit\n");
	printf("    -h             - Print this help\n");
	printf("    -i             - Initialize to defaults\n");
	printf("    -p             - Print current configuration\n");
	printf("    -P             - Trigger the power readout and print current configuration\n");
	printf("\n");
}

void msleep(unsigned int milliseconds)
{
	struct timespec st;
	st.tv_sec = milliseconds / 1000;
	st.tv_nsec = (milliseconds % 1000) * 1000000L;
	nanosleep(&st, NULL);
}

void print_error(int code)
{
	printf("%s\n", lo_error_string(code));
}

int print_conf(xildev *dev, bool trigger_conversion)
{
	int res;
	uint8_t config, atten;
	uint16_t clk_pow, lo_pow, ref_pow, version;
	bool ext_trigger = false;
	
	printf("\nFirmware version: ");
	res = lo_version_get(dev, &version);
	if(res == LO_ERROR_NONE)
		printf("0x%03X\n", version);
	else
		print_error(res);
	
	printf("\nLO config: ");
	res = lo_config_get(dev, &config);
	if(res == LO_ERROR_NONE)
	{
		printf("0x%02X\n", config);
			
		for(int i = 0; i < NUM_BITS; i++)
		{
			int value = (config >> i) & 1;
			const char *comment = value ? bits[i].on_comment : bits[i].off_comment;
			if(comment)
				printf("    cfg[%d] = %d  // %12s : %s\n", i, value, bits[i].name, comment);
			else
				printf("    cfg[%d] = %d  // %12s\n", i, value, bits[i].name);
		}
		
		ext_trigger = config & (1 << LO_BIT_POW_STR_SEL);
	}
	else
		print_error(res);

	if(trigger_conversion)
	{
		if(config & (1 << LO_BIT_POW_STR))
		{	// LO_BIT_POW_STR was set to "1", clearing it
			res = lo_config_clear_bits(dev, 1 << LO_BIT_POW_STR);
			if(res != LO_ERROR_NONE)
				print_error(res);
		}
		res = lo_config_set_bits(dev, 1 << LO_BIT_POW_STR);
		if(res != LO_ERROR_NONE)
			print_error(res);
		if(!(config & (1 << LO_BIT_POW_STR)))
		{	// LO_BIT_POW_STR was set to "0", clearing it
			res = lo_config_clear_bits(dev, 1 << LO_BIT_POW_STR);
			if(res != LO_ERROR_NONE)
				print_error(res);
		}
	}
	
	printf("\nAttenuation: ");
	res = lo_atten_get(dev, &atten);
	if(res == LO_ERROR_NONE)
		printf("0x%02X\n", atten);
	else
		print_error(res);

	printf("\nPower (");
	res = lo_power_get(dev, &clk_pow, &lo_pow, &ref_pow);
	if(res == LO_ERROR_NONE)
	{
		if(ext_trigger)
		{
			if(trigger_conversion)
				printf("readouts obtained on external trigger - manual trigger ignored");
			else
				printf("readouts obtained on external trigger");
		}
		else
		{
			if(trigger_conversion)
				printf("the conversion was manually triggered");
			else
				printf("measurements were not updated!");
		}
		printf("): \n");
		float clk_dbm = lo_power_in_dbm(clk_pow);
		float lo_dbm  = lo_power_in_dbm(lo_pow);
		float ref_dbm = lo_power_in_dbm(ref_pow);
		
		printf("  CLK: %5.1f dBm  // 0x%03X\n  LO:  %5.1f dBm  // 0x%03X\n  REF: %5.1f dBm  // 0x%03X\n",
			   clk_dbm, (int)clk_pow, lo_dbm, (int)lo_pow, ref_dbm, (int)ref_pow);
	}
	else
		print_error(res);
	printf("\n");
	
	return 0;
}

int set_bit(xildev *dev, const char *name, bool value)
{
	int res, bit_no = -1;
	for(int i = 0; i < NUM_BITS; i++)
	{
		if(strcmp(bits[i].name, name) == 0)
		{
			bit_no = i;
			break;
		}
	}
	if(bit_no < 0)
	{
		printf("Bit name %s does not match any of the known bits:\n", name);
		for(int i = 0; i < NUM_BITS; i++)
			printf("%s ", bits[i].name);
		printf("\n");
		return -1;
	}
	
	uint8_t config;
	res = lo_config_get(dev, &config);
	if(res != LO_ERROR_NONE)
	{
		print_error(res);
		return -1;
	}
	if(value)
		res = lo_config_set_bits(dev, 1 << bit_no);
	else
		res = lo_config_clear_bits(dev, 1 << bit_no);
	if(res != LO_ERROR_NONE)
	{
		print_error(res);
		return -1;
	}
	return 0;
}

int set_atten(xildev *dev, const char *value_txt)
{
	char *endptr = NULL;
	int value = strtol(value_txt, &endptr, 0);
	if(endptr == value_txt)
	{
		printf("Cannot parse '%s' as integer value\n", value_txt);
		return -1;
	}
	if(value < 0 || value > 31)
	{
		printf("Specified attenuation value is outside range of 0..31\n");
		return -1;
	}
	int res = lo_atten_set(dev, value);
	if(res != LO_ERROR_NONE)
	{
		print_error(res);
		return -1;
	}
	return 0;
}

int init(xildev *dev)
{
	printf("Releasing reset: ");
	int res = lo_config_set_bits(dev, (1 << LO_BIT_PARTIAL_RESET_N));
	if(res != LO_ERROR_NONE)
	{
		print_error(res);
		return -1;
	}
	printf("OK\nEnabling clocks: ");
	res = lo_config_set_bits(dev, (1 << LO_BIT_LE_EN) | (1 << LO_BIT_CLK_EN));
	if(res != LO_ERROR_NONE)
	{
		print_error(res);
		return -1;
	}
	printf("OK\nSetting attenuation: ");
	res = lo_atten_set(dev, 0);
	if(res != LO_ERROR_NONE)
	{
		print_error(res);
		return -1;
	}
	printf("OK\n");
	
	return 0;
}

int main(int argc, char *argv[])
{
	xildev *dev = NULL;
	int opt, result = -1;
	const char * dev_name = "/dev/xdma0";
	const char avail_options[] = "a:c:hipPs:x:";
	
	if(argc == 1)
	{
		printf("No arguments were given. Please execute \"%s -h\" for help.\n", argv[0]);
		return 0;
	}
	
	// Read configuration, handle getting help
	while((opt = getopt(argc, argv, avail_options)) != -1)
	{
		switch(opt)
		{
			case 'h':
				print_usage(argv[0]);
				return 0;
			case 'x':
				dev_name = optarg;
				break;
			case '?':
				print_usage(argv[0]);
				return -1;
		}
	}
	
	dev = xil_open_device(dev_name);
	if(!dev)
	{
		fprintf(stderr, "Cannot open file '%s' due to '%s'\n", dev_name, strerror(errno));
		return -1;
	}
	else
	{
		int init_s = lo_init(dev);
		if(init_s != LO_ERROR_NONE)
		{
			printf("Warning: LO initialization error - ");
			print_error(init_s);
			printf("Most probable causes:\n");
			printf(" - dead PCIe link (maybe reboot is needed?)\n");
			printf(" - wrong board selected (consider passing -x option)\n");
			xil_close_device(dev);
			return -1;
		}
	}
	
	// Perform tasks
	optind = 1;
	while((opt = getopt(argc, argv, avail_options)) != -1)
	{
		switch(opt)
		{
		case 'a':
			result = set_atten(dev, optarg);
			break;
		case 'c':
			result = set_bit(dev, optarg, false);
			break;
		case 'i':
			result = init(dev);
			break;
		case 'p':
			result = print_conf(dev, false);
			break;
		case 'P':
			result = print_conf(dev, true);
			break;
		case 's':
			result = set_bit(dev, optarg, true);
			break;
		}
	}
	
	xil_close_device(dev);
	return result;
}
