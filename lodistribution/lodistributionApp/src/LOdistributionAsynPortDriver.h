#include "asynPortDriver.h"

#include "liblo.h"

#define NUM_BITS 8

class LOdistributionAsynPortDriver: public asynPortDriver {
	public:
		LOdistributionAsynPortDriver(const char *portName, int maxPoints, const char *devName);
		~LOdistributionAsynPortDriver();

		void LOdistributionIrq(void);
		asynStatus writeInt32(asynUser *pasynUser, epicsInt32 value);

		void setAttenuation();
		void readConfig(int *array_value);
		void readAttenuation(int pv_param);
		void readPower();
		void initArrays();
		void setBit(int idx);

		void AlarmProcedure(int result);
	protected:
		// parameters
		int P_CONFIG_TRIG;
		int P_POWER_TRIG;
		int P_ATTN_TRIG;
		int P_VERSION_RB;
		int P_CONFIG_RB;
		struct{
			int VAL_RB;
			int VAL_SP;
			int NAME_RB;
			int COMMENT_RB;
			} P_LO_STATE, P_PART_RST, P_DIV, P_BOOST_TYPE, P_CLK_STATE, P_DIV_TYPE, P_TRIG_SOUR, P_TRIG;
		int P_ATTENUATION_RB;
		int P_ATTENUATION_SP;
		int P_CLK_POWER_RB;
	 	int P_CLK_DBM_RB;
		int P_LO_POWER_RB;
		int P_LO_DBM_RB;
		int P_REF_POWER_RB;
		int P_REF_DBM_RB;
		int P_DMA_RB;	
		int P_ALARM;
		// arrays with comments, names, values-rd, values-sp, and bool flags 
		int array_bits_comment[NUM_BITS];
		int array_bits_val[NUM_BITS];
		int array_bits_name[NUM_BITS];
		int array_bits_val_sp[NUM_BITS];
		bool array_bits_bool[NUM_BITS];		
	private:
		// bit description
		struct BitDescr {
				const char *name;
				const char *on_comment;
				const char *off_comment;
				} const bits[NUM_BITS] = {
					{"LO_EN", "LO enabled", "LO disabled"},
					{"PARTIAL_RESET_N", "Reset released", "Reset active"},
					{"IF_SEL", "IF divider 22", "IF divider 28"},
					{"IF_BOOST", "IF: HSTL Boost", "IF: HSTL"},
					{"CLK_EN", "CLK enabled", "CLK disabled"},
					{"CLK_CFG", "alternate dividers (not recommended)", "default dividers"},
					{"POW_STR_SEL", "power measurement triggered by Zone3", "power measurement triggered by POW_STR bit"},
					{"POW_STR", "continuous power readout", "triggered power readout"},
				};
		// device
		xildev *dev = NULL;
};

