#############################
### general configuration
#############################

epicsEnvSet("EPICS_CA_MAX_ARRAY_BYTES", "1000000")
epicsEnvSet("DISPLAY_LOG", "1")

#############################
### load lo configuration
#############################
require lodistribution

epicsEnvSet("P", "LOdistribution:")
epicsEnvSet("R", "asyn:")
epicsEnvSet("PORT", "LOPort")
epicsEnvSet("DEVICE_NAME", "/dev/xdma_lo1") 

loadIocsh("$(lodistribution_DIR)/lo.iocsh", "P=$(P), R=$(R), PORT=$(PORT), DEVICE_NAME=$(DEVICE_NAME), TIMEOUT=1")

#########################
iocInit()
eltc "$(DISPLAY_LOG)"

